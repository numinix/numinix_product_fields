<?php 

$zc156 = (PROJECT_VERSION_MAJOR > 1 || (PROJECT_VERSION_MAJOR == 1 && substr(PROJECT_VERSION_MINOR, 0, 3) >= 5.6));
if($zc156){ ?>
          <div class="form-group">
              <?php echo zen_draw_label(TEXT_PRODUCTS_DISALLOWED_ZONE, 'disallowed_zone', 'class="col-sm-3 control-label"'); ?>
            <div class="col-sm-9 col-md-6">
                <?php echo str_replace('<select name="disallowed_zone">', '<select name="disallowed_zone"><option value="0">' . TEXT_SELECT_AN_OPTION . '</option>', zen_geo_zones_pull_down('name="disallowed_zone"', $pInfo->disallowed_zone)); ?>
            </div>
          </div>
<?php } else { ?>
          <tr>
            <td colspan="2"><?php echo zen_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
          </tr>          
          <tr bgcolor="#DDEACC">
            <td class="main"><?php echo TEXT_PRODUCTS_DISALLOWED_ZONE; ?></td>
            <td class="main"><?php echo zen_draw_separator('pixel_trans.gif', '24', '15') . '&nbsp;' . str_replace('<select name="disallowed_zone">', '<select name="disallowed_zone"><option value="0">' . TEXT_SELECT_AN_OPTION . '</option>', zen_geo_zones_pull_down('name="disallowed_zone"', $pInfo->disallowed_zone)); ?></td>
          </tr>
          <tr>
            <td colspan="2"><?php echo zen_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
          </tr>
<?php } ?>
